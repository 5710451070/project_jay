import React from 'react'
import Footer from '../components/Footer'
import Header from '../components/Header'

const About = () => {
    return (

        <div>
            <Header/>
            <div className="container col-md-5">
                <h3>สวัสดีครับ</h3>
                <p className="title text-justify mt-4 mb-4">
                    เราคือตัวแทนจำหน่ายลูกชิ้นราคาส่ง ส่งตรงจากโรงงาน   
                </p>
                <h4 className="text-success" >จาก ลูกชิ้น นายเจ</h4>
            </div>
            <Footer/>
        </div>
    )
}

export default About;