import React, { Component } from "react";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import { connect } from "react-redux";
import { orderFetch, orderDelete } from "../../actions/";
import { Link } from "react-router-dom";
import axios from "axios";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import Button from '@material-ui/core/Button';



function Transition(props) {
    return <Slide direction="up" {...props} />;
  }

class Order extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      data : []
    };
  }

  async componentDidMount() {
     this.props.orderFetch();
    const resultt = await axios.get("http://localhost:3001/login");
    console.log(resultt.data)
    this.setState({data :  resultt.data})
  }

  delOrder(order) {
    console.log(10);
    this.props.orderDelete(order.id);
  }

  confirmOrder = async ({ target }) => {
    console.log(target.getAttribute("value"));
    console.log(target);
    let key = target.getAttribute("id");
    const resultt = await axios.get("http://localhost:3001/orders");
    let a = resultt.data;
    let b = a.find(val => val.id == key);
    console.log(b);
    if (b.check === "จัดส่งสินค้าเรียบร้อยแล้ว") {
      b.check = "ยังไม่จัดส่งสินค้า";
    } else {
      b.check = "จัดส่งสินค้าเรียบร้อยแล้ว";
    }

    const result = await axios.put("http://localhost:3001/orders/" + key, b);
    this.props.orderFetch();
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  
  displayModal = (order) => {
      return this.state.data && this.state.data.map(val=>{
        return order.login === val.name && (
         
         <Dialog
         maxWidth="sm"
         fullWidth={true}
         open={this.state.open}
         TransitionComponent={Transition}
         keepMounted
         onClose={this.handleClose}
         aria-labelledby="alert-dialog-slide-title"
         aria-describedby="alert-dialog-slide-description"
       >
         <DialogTitle id="alert-dialog-slide-title">
            ประวัติส่วนตัว
         </DialogTitle>
         <DialogContent>
           <DialogContentText id="alert-dialog-slide-description">
                ชื่อ : {val.name}
           </DialogContentText>
           <DialogContentText id="alert-dialog-slide-description">
                อีเมล : {val.email}
           </DialogContentText>
           <DialogContentText id="alert-dialog-slide-description">
                เบอร์ : {val.tel}
           </DialogContentText>
           <DialogContentText id="alert-dialog-slide-description">
                ที่อยู่ : {val.address}
           </DialogContentText>
         </DialogContent>
         <DialogActions>

           <Button onClick={this.handleClose} color="primary">
             ยืนยัน
           </Button>
         </DialogActions>
       </Dialog>
     )})
  }
  showOrder() {
    
    return (
      this.props.orders &&
      this.props.orders.map(order => {
        const date = new Date(order.orderDate);
        if (order.login === this.props.login) {
          return (
            <div key={order.id} className="col-md-3">
              <hr />
              <p className="text-right">
                <button
                  className="btn btn-danger btn-sm title"
                  onClick={() => this.delOrder(order)}
                >
                  X
                </button>
              </p>
              <h5>
                วันที่ {date.toLocaleDateString()} {date.toLocaleTimeString()}
              </h5>
              <ul>
                {order.orders &&
                  order.orders.map(record => {
                    return (
                      <li key={record.product.productName}>
                        {record.product.productName} x {record.quantity} ={" "}
                        {record.product.unitPrice * record.quantity}
                      </li>
                    );
                  })}
              </ul>
              <p className="title">ยอดรวม {order.totalPrice}</p>
              {order.check === "จัดส่งสินค้าเรียบร้อยแล้ว" ? (
                <p className="button" style={{ color: "green" }}>
                  {order.check}
                </p>
              ) : (
                <p className="button" style={{ color: "red" }}>
                  {order.check}
                </p>
              )}
            </div>
          );
        } else if (this.props.login === "Chayapol VICHAYA-ORANKUL") {
          return (
            <div key={order.id} className="col-md-3">
              <hr />
              <p className="text-right">
                <button
                  className="btn btn-danger btn-sm title"
                  onClick={() => this.delOrder(order)}
                >
                  X
                </button>
              </p>
              <h5>
                วันที่ {date.toLocaleDateString()} {date.toLocaleTimeString()}
              </h5>
              <ul>
                {order.orders &&
                  order.orders.map(record => {
                    return (
                      <li key={record.product.productName}>
                        {record.product.productName} x {record.quantity} ={" "}
                        {record.product.unitPrice * record.quantity}
                      </li>
                    );
                  })}
              </ul>
              <p className="title">ยอดรวม {order.totalPrice}</p>
              <p className="button" style={{ color: "green" }}>
                สินค้าของ {order.login}
              </p>
              {order.check !== "จัดส่งสินค้าเรียบร้อยแล้ว" ? (
                <p
                  className="btn btn-danger "
                  id={order.id}
                  value={order.login}
                  onClick={this.confirmOrder}
                  style={{ color: "white" }}
                >
                  {order.check}
                </p>
              ) : (
                <p
                  className="btn btn-success "
                  id={order.id}
                  value={order.login}
                  onClick={this.confirmOrder}
                  style={{ color: "white" }}
                >
                  {order.check}
                </p>
              )}
              <p
                className="btn btn-info ml-2"
                style={{ color: "white" }}
                onClick={this.handleClickOpen}
              >
                ประวัติส่วนตัว
              </p>
              {this.displayModal(order)}
             
              
            </div>
          );
        }
      })
    );
  }
  render() {
    return (
      <div>
        <Header />
        <div className="container-fluid">
          <h1>รายการสั่งซื้อ</h1>
          <div className="row">{this.showOrder()}</div>
        </div>
        <Footer />
      </div>
    );
  }
}

function mapStateToProps({ orders, login }) {
  return { orders, login };
}

export default connect(
  mapStateToProps,
  { orderFetch, orderDelete }
)(Order);
