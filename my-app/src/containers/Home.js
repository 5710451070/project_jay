
import React, { Component } from 'react';
import Header from "../components/Header"
import Footer from "../components/Footer"
import Monitor from '../components/monitor/Monitor';
import {connect} from "react-redux";
import {productsFetch} from "../actions"
import axios from 'axios'

class Home extends Component {

  constructor(props){
    super(props);
    this.state = { pictures: [] };
  }

  componentDidMount(){
    this.props.productsFetch();
    console.log(this.props.products)
    axios.get("http://localhost:3001/products").then(res => {
            console.log(res.data)
        })
      console.log(this.props.login)
  }


  

  
  render() {
    return (
      <div className="Home">
          <Header/>
          <Monitor products={this.props.products}/>
          <Footer company="Olanlab" email="olan@olanlab.com"/>
      </div>
    );
  }
}

function mapStateToProps({products , login}) {
  return {products ,login}
 // return {product : state.products}
}

export default connect(mapStateToProps, {productsFetch})(Home);
