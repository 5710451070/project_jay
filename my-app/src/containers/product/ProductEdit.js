import React , { Component } from "react";
import {connect} from "react-redux"
import {productCreate , productUpdate , productFetch } from "../../actions/ProductActions"
import Header from "../../components/Header"
import Footer from "../../components/Footer"
import ProductForm from "../../components/product/ProductForm"


class ProductEdit extends Component {

    componentDidMount() {
        if (this.props.match.params.id) {
            this.props.productFetch(this.props.match.params.id)
        }
    }
    render() {
        const {match , formValues , products , productCreate , productUpdate , thumbnail} = this.props;
          let a = formValues
          a = {...a , thumbnail}
     
          console.log(a)
          console.log(products)
        return (
            <div>
                <Header/>
                <div className="container col-md-5">
                    {match.path.indexOf('add') > 0 && (
                        <div>
                            <h2>เพิ่ม</h2>
                            {products.save && (
                                <div className="alert alert-secondary title" role="alert">
                                {products.msg}
                                </div>
                            )}
                            <ProductForm onProductSubmit={() => productCreate(a)}/> 
                        </div>
                    )}

                    {match.path.indexOf('edit') > 0 && (
                        <div>
                            <h2>แก้ไข</h2>
                            {products.saved && (
                                <div className="alert alert-secondary title" role="alert">
                                {products.msg}
                                </div>
                            )}
                            <ProductForm onProductSubmit={() => productUpdate(products.id, a)}/> 
                        </div>
                    )}
                
                </div>
                <Footer/>

            </div>
        )
    }
}

function mapStateToProps ({form , products ,login , thumbnail}) {

  
    return {formValues : form.productForm ? form.productForm.values : null , products , thumbnail}

}
export default connect(mapStateToProps, {productCreate,productFetch,productUpdate} )(ProductEdit);