import React, { Component } from "react";
import Footer from "../components/Footer";
import Header from "../components/Header";
import firebase from "firebase/app";
import "firebase/auth";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import { connect } from "react-redux";
import { LoginFetch } from "../actions";
import axios from "axios";

const firebaseConfig = {
  apiKey: "AIzaSyDPFJPQGorfCtwNFCa6Eeubay8hgxUgYyA",
  authDomain: "project-jay-d886b.firebaseapp.com",
  databaseURL: "https://project-jay-d886b.firebaseio.com",
  projectId: "project-jay-d886b",
  storageBucket: "project-jay-d886b.appspot.com",
  messagingSenderId: "527521474525"
};

// Instantiate a Firebase app.
firebase.initializeApp(firebaseConfig);

class Login extends Component {
  state = {
    isSignedIn: false,
    check: false,
    name: null,
    tel: null,
    address: null,
    data: null,
    check : false,
    test : {}
    // Local signed-in state.
  };
  uiConfig = {
    // Popup signin flow rather than redirect flow.
    signInFlow: "popup",
    // We will display Google , Facebook , Etc as auth providers.
    signInOptions: [firebase.auth.GoogleAuthProvider.PROVIDER_ID],
    callbacks: {
      // Avoid redirects after sign-in.
      signInSuccess: () => false
    }
  };
  componentDidMount() {
    this.setState({check : false})
    console.log(200);
    this.unregisterAuthObserver = firebase
      .auth()
      .onAuthStateChanged(user => this.setState({ isSignedIn: !!user }));
  }

  async componentDidUpdate(prevProps, prevState) {
    let { isSignedIn } = this.state;
    console.log(prevState.isSignedIn);
    console.log(isSignedIn);
    if (prevState.isSignedIn !== this.state.isSignedIn) {
      if (isSignedIn) {
       
      
        // let a = {
        //   name: firebase.auth().currentUser.displayName
        // };
        const result = await axios.get("http://localhost:3001/login");
        // console.log(result.data);
        let dd = result.data;
        let aa = dd.find(val => {
          return val.email === firebase.auth().currentUser.email;
        });
        if (firebase.auth().currentUser.displayName === "Chayapol VICHAYA-ORANKUL") {
          this.props.LoginFetch(firebase.auth().currentUser.displayName);
          this.setState({ data: aa ,name : aa.name , tel : aa.tel , address : aa.address});
        }
        else{
          if (aa){
            this.props.LoginFetch(aa.name);
            this.setState({ data: aa ,name : aa.name , tel : aa.tel , address : aa.address});
          }
          else{
            this.setState({name : firebase.auth().currentUser.displayName})
          }
          
          console.log(this.state.test)
        }
      
        // console.log(aa);

        // if (!aa) {
        //   const result = await axios.post("http://localhost:3001/login", a);
        // }
      } else {
        this.props.LoginFetch("");
      }
    }
  }

  handleChange = ({ target }) => {
    this.setState({ [target.name]: target.value });
  };

  submit = async e => {
    // e.preventDefault();
    let { name, tel, address ,check} = this.state;
    let test = { name, tel, address };
    test = {...test , 'email' : firebase.auth().currentUser.email}
    this.setState({test})
    if (check) {
      const result = await axios.patch("http://localhost:3001/login/"+this.state.data.id, test);
    }
    else {
      const result = await axios.post("http://localhost:3001/login", test);
    }
    
  };

  // Make sure we un-register Firebase observers when the component unmounts.
  componentWillUnmount() {
    console.log(20);
    this.unregisterAuthObserver();
  }
  edit = () =>{
    this.setState({check : true})
    console.log(this.state.data)
  }
  render() {
    console.log(this.state.data)
    console.log(this.state.check)
    if (!this.state.isSignedIn) {
      return (
        <div>
          <Header />
          <h1 className="text-center">Sign In ผ่าน google</h1>

          <p className="text-center">Please sign-in:</p>
          <StyledFirebaseAuth
            uiConfig={this.uiConfig}
            firebaseAuth={firebase.auth()}
          />
          <Footer />
        </div>
      );
    }
    return (
      <div className="">
        <Header />
        {(!this.state.data || this.state.check) ? (
          <div className="container col-md-5">
            <div className="form-group">
              <h2 className="text-center">ประวัติส่วนตัว</h2>
              <form onSubmit={this.submit}>
                <label className="title mt-3">ชื่อ</label>
                <input
                  type="text"
                  required={true}
                  name="name"
                  value={this.state.name}
                  onChange={this.handleChange}
                  className="form-control"
                />

                <label className="title text-left mt-3">เบอร์โทรศัพท์</label>
                <input
                  type="text"
                  required={true}
                  name="tel"
                  value={this.state.tel}
                  onChange={this.handleChange}
                  className="form-control"
                />

                <label className="title text-left mt-3">ที่อยู่</label>
                <input
                  type="text"
                  required={true}
                  name="address"
                  value={this.state.address}
                  onChange={this.handleChange}
                  className="form-control"
                />

                <button
                  className="btn btn-block btn-info title mt-5"
                  type="submit"
                >
                  บันทึก
                </button>
              </form>
            </div>
          </div>
        ) : (
          <div className="container col-md-12 text-center">
            <div className="row">
            <div className="col-md-7 text-right">
              <h1>Login สำเร็จ</h1>
            </div>
            <div className="col-md-5 text-right">
            <button
                  className="btn btn-primary title"
                  type="submit"
                  onClick={this.edit}
                >
                  แก้ไขประวัติ
                  
                </button>
            </div>
            </div>
         

            <img
              id="photo"
              className="pic"
              src={firebase.auth().currentUser.photoURL}
              style={{ height: 200 }}
            />
            <button
              className="btn  btn-info title ml-5"
              onClick={() => firebase.auth().signOut()}
            >
              Sign-out
            </button>
          </div>
        )}

        <Footer />
      </div>
    );
  }
}

function mapStateToProps({ login }) {
  return { login };
  // return {product : state.products}
}
export default connect(
  mapStateToProps,
  { LoginFetch }
)(Login);
