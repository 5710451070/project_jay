import React , {Component} from 'react'
import {connect} from "react-redux"
import {reduxForm , Field} from 'redux-form'
import FormField from "../monitor/common/FormField"
import { loginFormField } from "./formFields";
import ImageUpload from '../ImageUpload'

import {getImage} from "../../actions/ImageActions"

class LoginForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            test : "images/product/"
        }
        
    }

    renderFields(formFields){
        return formFields.map(({label ,name , type , required} ) => {
            return (
                <Field key={name} label={label} name={name} type={type} required={required} component={FormField}/>
            )
        })
    }

    getImage = async (data) => {
        console.log(data.name)
        let {test} = this.state
        if (data) {
            console.log("SSS")
            console.log(data)
            await this.setState({test : this.state.test+data.name})
            await this.props.getImage(this.state.test)
            console.log(this.props.image)
        }

    
        
    }

    render() {
        const { onProductSubmit } = this.props
        return (
            <div>
                <form onSubmit={this.props.handleSubmit(onProductSubmit)}>
                    {this.renderFields(loginFormField)}
                    <ImageUpload data={this.getImage}/>
                    <button className="btn btn-block btn-info title" type="submit">
                        บันทึก
                    </button>
                </form>
            </div>
        )

    }
    
}

function validate(values){
    //console.log(values)
    const errors = {}
    loginFormField.forEach(({name , required }) => {
    
        if (!values[name] && required) {
            errors[name] = "กรุณากรอกข้อมูลด้วย"

        }
    })
    return errors;
}

function mapStateToProps({products,thumbnail}) {
   // console.log(products)
    if (products && products.id) {

        return {initialValues : products }
    }
    else{
        return {thumbnail}
    }

   
   
   
}

LoginForm = reduxForm({ validate ,form : "LoginForm"})(LoginForm);

export default connect(mapStateToProps)(LoginForm);