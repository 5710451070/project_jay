import React, { Component } from "react";
import {Link} from "react-router-dom"
import {connect} from "react-redux";
class Header extends Component {
  constructor(props) {
    super(props);
    this.state = { date: new Date() };
  }

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000);
    console.log(this.props.login)
  }

  componentDidUpdate() {}

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({ date: new Date() });
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-7 text-left">
            <h1 className="text-success">
            <img style = {{height: 80 , marginTop : 10}}src="/images/logo/main.png" alt="" /> ลูกชิ้น นายเจ
            </h1>
          </div>
          <div className="col-md-5 text-right">
            {this.props.login !== "" && <h5 className="mt-2">สวัสดีคุณ {this.props.login} {this.props.login === "Chayapol VICHAYA-ORANKUL" && "(Admin)"}</h5>}
            <h5 className="text-muted mt-4">{this.state.date.toLocaleTimeString()}</h5>
            <ul className="list-inline">
                <li className="list-inline-item title"><Link className="text-success" to="/">หน้าหลัก</Link></li>
                <li className="list-inline-item title">|</li>
                <li className="list-inline-item title"><Link className="text-success" to="/orders">รายการสั่งซื้อ</Link></li>
                <li className="list-inline-item title">|</li>
                <li className="list-inline-item title"><Link className="text-success" to="/products">สินค้า</Link></li>
                <li className="list-inline-item title">|</li>
                <li className="list-inline-item title"><Link className="text-success" to="/about">เกี่ยวกับเรา</Link></li>
                <li className="list-inline-item title">|</li>
                {this.props.login !== "" ? <li className="list-inline-item title"><Link className="text-success" to="/login">ออกจากระบบ</Link></li> : <li className="list-inline-item title"><Link className="text-success" to="/login">เข้าสู่ระบบ</Link></li>}
              
                
            
            </ul>
          </div>
        </div>
        <hr/>
      </div>
    
    );
  }
}


function mapStateToProps({login}) {
  return {login}
 // return {product : state.products}
}
export default connect(mapStateToProps)(Header);
