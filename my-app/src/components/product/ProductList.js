import React , {Component} from "react";
import ProductItem from './ProductItem';


class ProductList extends Component{
    constructor(props) {
        super(props)
        this.state = {
            order : props.orders,
            data : 10
        }
    }
    

    showProduct(order){
            return ((Array.isArray(this.props.product) && this.props.product ) || this.state.data==10) && this.props.product.map(product => (
                <ProductItem  key={product.id} product={product} onAddOrder={this.props.onAddOrder} onDelProduct={this.props.onDelProduct} onEditProduct={this.props.onEditProduct}/>
            ))}
    
        render(){

        return (
            <div className="row">
                {this.showProduct(this.props.orders)}

            </div>
        )
    }
}

export default ProductList;