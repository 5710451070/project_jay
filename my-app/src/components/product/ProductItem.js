import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios"
class ProductItem extends Component {
  state = {
    test: 0,
    bb: this.props.product.onHand
  };
  doSomething(productName) {}

  componentDidUpdate(prevProps, prevState) {
    const { productName, unitPrice, thumbnail, onHand } = this.props.product;

    let a = this.props.orderNa.find(val => {
      return val.product.productName === productName;
    });
    let bb = onHand;
    if (a) {
      bb = onHand - a.quantity;
      console.log(this.state.bb);
      console.log(bb);
      console.log(prevProps.product.onHand);

      if (this.state.bb != prevProps.product.onHand) {
        console.log(this.props.orderNa);
        console.log(2222222222);
        this.setState({ bb });
      }

      // if (prevProps.product.onHand != this.state.bb) {
      //   this.setState({bb})
      // // }

      // }
    }
  }

  handle = async () => {
    this.props.onAddOrder(this.props.product);
    console.log(this.props.product)
    let result = await axios.get("http://localhost:3001/products")
    console.log(result)
    this.setState({ test: this.state.test + 1 });
  };

  displayButton = () => {
    const { productName, unitPrice, thumbnail, onHand } = this.props.product;
    let a = this.props.orderNa.find(val => {
      return val.product.productName === productName;
    });
    let bb = onHand;
    if (a) {
      bb = onHand - a.quantity;
    }
    if (this.props.onAddOrder) {
      if (bb > 0) {
        return (
          <button
            className="btn btn-block btn-success title"
            onClick={this.handle}
          >
            ซื้อ
          </button>
        );
      } else {
        return (
          <button
            disabled
            className="btn btn-block btn-danger title"
            onClick={this.handle}
          >
            สินค้าหมด
          </button>
        );
      }
    }
  };

  render() {
    const { productName, unitPrice, thumbnail, onHand } = this.props.product;
    let value = 0;

    let a = this.props.orderNa.find(val => {
      return val.product.productName === productName;
    });
    console.log(this.props.orderNa);
    console.log(a);
    let bb = onHand;
    // if (!a) {
    // //     console.log(10)
    // //   this.props.orderNa.map(val => {
    // //     bb = val.product.onHand;
    // //   });
    //     for (let i in this.props.orderNa) {
           
    //         if (this.props.orderNa[i].quantity > 0) {
    //             bb = this.props.orderNa[i].onHand - this.props.orderNa[i].quantity
    //         }
    //     }
  // }
    // if (a) {
    //      bb = onHand - a.quantity
    //     console.log(bb)
    // }
    // else if (this.props.orderNa)
    // else if (!a) {
    //     this.props.orderNa.map(val => {
    //         bb = val.product.onHand
    //     })
    // }

    return (
      <div className="col-md-3 col-sm-6">
        <img
          className="img-fluid img-thumbnail"
          src={thumbnail}
          style={{ height: 200 }}
        />
        <h5 className="mt-2">{productName}</h5>
        <div className="row">
          <div className="col-md-6">
            <p className="title text-left">{a ? bb - a.quantity : bb} ชิ้น</p>
          </div>
          <div className="col-md-6">
            <p className="title text-right">{unitPrice} THB</p>
          </div>
        </div>

        {this.displayButton()}

        {(this.props.onDelProduct || this.props.onEditProduct) &&
          this.props.login == "Chayapol VICHAYA-ORANKUL" && (
            <button
              className="btn btn-info col-5 title"
              onClick={() => this.props.onEditProduct(this.props.product)}
            >
              แก้ไข
            </button>
          )}
        {(this.props.onDelProduct || this.props.onEditProduct) &&
          this.props.login == "Chayapol VICHAYA-ORANKUL" && (
            <button
              className="btn btn-danger col-5 float-right title"
              onClick={() => this.props.onDelProduct(this.props.product)}
            >
              ลบ
            </button>
          )}

        <hr />
      </div>
    );
  }
}
function mapStateToProps({ login, orderNa }) {
  return { login, orderNa };
  // return {product : state.products}
}
export default connect(mapStateToProps)(ProductItem);
