import React , {Component} from "react";
import Calculator from './Calculator';
import ProductList from '../product/ProductList'
import axios from 'axios';
import {connect} from "react-redux";
import {getOrder} from '../../actions/OrderActions'
class Monitor extends Component{
    constructor(props){
        super(props);
        this.state = {totalPrice : 0 , orders : [],confirm : false,msg : "" , check : "ยังไม่จัดส่งสินค้า"};
        this.addOrder = this.addOrder.bind(this)
        this.delOrder = this.delOrder.bind(this)
        this.cancelOrder = this.cancelOrder.bind(this)
        this.confirmOrder = this.confirmOrder.bind(this)
    
    }

    async addOrder(product){
        let {login} = this.props
        let fineOrder = this.state.orders.find(order => order.product.id === product.id);
        if (login === "") {
            alert("กรุณาเข้าสู่ระบบ")
        }
        else if (login === "Chayapol VICHAYA-ORANKUL") {
            alert("คุณเป็น Admin")
        }
        else{
            if (fineOrder){
                fineOrder.quantity++;
            }
            else{
                this.state.orders.push({product: product,quantity: 1})
            }
            console.log(product)
            const totalPrice = this.state.totalPrice + parseInt(product.unitPrice);
            await this.props.getOrder(this.state.orders)
            console.log(this.state.orders)
            console.log(this.props.orderNa)
           
            this.setState({totalPrice: totalPrice, orders:this.state.orders, confirm : false })
        }
       
       
       // console.log(fineOrder)
    }

    async delOrder(product){
        console.log(product)
        let findOrder = this.state.orders.find(order => order.product.id === product.id);
        let resultOrder = this.state.orders.filter(order => order.product.id !== product.id);
     
        const totalPrice = this.state.totalPrice - (findOrder.quantity * parseInt(findOrder.product.unitPrice))
        this.setState({totalPrice: totalPrice , orders:resultOrder , confirm : false })
        await this.props.getOrder(resultOrder)
    }

    async cancelOrder(product){
        let findOrder = this.state.orders.find(order => order.product.id === product.id);
        let resultOrder = this.state.orders.filter(order => order.product.id !== product.id);
        resultOrder.map(val => {
            val.quantity = 0
        })
        console.log(resultOrder)
     
        this.setState({totalPrice: 0 , orders : []})
        await this.props.getOrder(resultOrder)
     
       
    }

    async confirmOrder(){
        const {totalPrice,orders , check} = this.state;
        if (orders && orders.length > 0){
           
            axios.post("http://localhost:3001/orders", {login : this.props.login , orderDate : new Date() , totalPrice, orders , check })
            .then(res =>  {
                this.setState({totalPrice: 0 , orders : [], confirm : true , msg : "บันทึกรายการสั่งซื้อเรียบร้อยแล้ว" })


            })
            let test = await axios.get("http://localhost:3001/products")
            let a = test.data
            let tt = null
            for (let i in a) {
               
                for ( let j in orders) {
                    if (orders[j].product.id == a[i].id) {
                        a[i].onHand = a[i].onHand - orders[j].quantity
                        console.log(a[i])
                        let test = await axios.patch("http://localhost:3001/products/"+a[i].id , a[i])
                    }
                }
            }
            
        }
        else{
            this.setState({totalPrice: 0 , orders : [], confirm : true , msg : "เลือกสินค้าก่อน" })
        }
       
    }

   

    render(){
        return (
            <div className="container-fluid">
             {this.state.confirm && 
                 <div className="alert alert-secondary title text-right" role="alert">
                    {this.state.msg}
                 </div>
            }
            

                <div className='row'>
                    <div className="col-md-9">
                        <ProductList product={this.props.products} onAddOrder={this.addOrder} /> 
                    </div>
                    <div className="col-md-3">
                        <Calculator totalPrice={this.state.totalPrice} orders={this.state.orders} onDeleteOrder={this.delOrder} onCancelOrder={this.cancelOrder} onConfirmOrder={this.confirmOrder}/>
                    </div>
                </div>
         

            </div>
        )
    }
}

function mapStateToProps({login , orderNa}) {
    return {login , orderNa}
   // return {product : state.products}
  }
export default connect(mapStateToProps , {getOrder})(Monitor);