import React from "react";

const Footer = (props) => {
    const {company,email} = props
    return (
        <div className="container-fluid">
            <hr/>
            <div className="text-center title text-uppercase">
            <small>
            <span className="text-danger">สนับสนุนโดย : ลูกชิ้น นายเจ</span> | <span>Contact By Email : chayapol.v@ku.th</span> 
            </small>
            </div>
        
        </div>
    )
}

export default Footer;