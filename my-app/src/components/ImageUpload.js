import React from 'react'
import axios from 'axios'
import './ImageUpload.css'



const endpoint = 'http://localhost:3002/upload'
class ImageUpload extends React.Component {
    constructor(props) {
      super(props);
      this.state = {file: '',imagePreviewUrl: ''};
    }
  
    _handleSubmit(e) {
      e.preventDefault();
     
      // TODO: do something with -> this.state.file

      console.log('handle uploading-', this.state.file);
    }
  
    async _handleImageChange(e) {
      e.preventDefault();
  
      let reader = new FileReader();
      let file = e.target.files[0];
  
      reader.onloadend = () => {
        this.setState({
          file: file,
          imagePreviewUrl: reader.result
        });
      }
      
      //console.log(file)
      if (file) {
        reader.readAsDataURL(file)
        const data = new FormData()
        this.props.data(file)
        data.append('file', file)
  
        const result = await axios
          .post(endpoint, data)
        
      }

   
    }
  
    render() {
      let {imagePreviewUrl} = this.state;
      let $imagePreview = null;
      if (imagePreviewUrl) {
      
        $imagePreview = (<img className="testPic" src={imagePreviewUrl} />);
      } else {
        $imagePreview = (<div className="previewText">Please select an Image for Preview</div>);
      }
  
      return (
        <div className="previewComponent">
          <form onSubmit={(e)=>this._handleSubmit(e)}>
            <input className="fileInput" 
              type="file" 
              onChange={(e)=>this._handleImageChange(e)} />

          </form>
          <div className="imgPreview">
            {$imagePreview}
          </div>
        </div>
      )
    }
  }
    
export default ImageUpload;