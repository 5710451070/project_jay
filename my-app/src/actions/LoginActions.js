import { LOGIN_USER } from './types'

export const LoginFetch = (data) => {

    return dispatch => {
         
            dispatch({type : LOGIN_USER , payload : data});
     
    }
}

