export const ORDERS_FETCH = "orders_fetch"

export const PRODUCTS_FETCH = "products_fetch"

export const PRODUCT_FETCH = "product_fetch"
export const PRODUCT_CREATE = "product_create"
export const PRODUCT_UPDATE = "product_update"

export const LOGIN_USER = "login_user"

export const IMAGE = "image"