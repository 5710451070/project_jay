import {combineReducers} from "redux";
import ProductReducer from "./ProductReducer"
import OrderReducer from "./OrderReducer";
import {getOrder} from "./OrderReducer"
import LoginReducer from "./LoginReducer";
import ImageRuducer from "./ImageReducer";
import {reducer as reduxFrom} from "redux-form"

const rootReducer = combineReducers({
    orders : OrderReducer,
    orderNa : getOrder,
    products : ProductReducer,
    login : LoginReducer,
    thumbnail : ImageRuducer,
    form : reduxFrom
})

export default rootReducer;