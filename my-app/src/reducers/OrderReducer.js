

import {ORDERS_FETCH} from '../actions/types'
export default function(state = [] , action) {
    switch (action.type){
        case ORDERS_FETCH :
            return action.payload;
        default:
            return state;

    }
}

export function getOrder(state = [] , action) {
    switch (action.type){
        case "GET_ORDER" :
            console.log(action.payload)
            return action.payload
        default:
            return state;

    }
}